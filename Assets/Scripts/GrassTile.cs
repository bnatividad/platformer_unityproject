﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GrassTile : Tile
{
	[SerializeField] private Sprite[] grassSprites;
	[SerializeField] private Sprite preview;

	[Space (10)]
	[SerializeField] private Sprite singleSprite;
		
	[Space (10)]
	[SerializeField] private Sprite topLeftSprite;
	[SerializeField] private Sprite topSprite;
	[SerializeField] private Sprite topRightSprite;
	[SerializeField] private Sprite leftSprite;
	[SerializeField] private Sprite middleSprite;
	[SerializeField] private Sprite rightSprite;
	[SerializeField] private Sprite bottomLeftSprite;
	[SerializeField] private Sprite bottomSprite;
	[SerializeField] private Sprite bottomRightSprite;

	[Space (10)]
	[SerializeField] private Sprite leftSingleRowSprite;
	[SerializeField] private Sprite middleSingleRowSprite;
	[SerializeField] private Sprite rightSingleRowSprite;

	[Space (10)]
	[SerializeField] private Sprite topSingleColumnSprite;
	[SerializeField] private Sprite middleSingleColumnSprite;
	[SerializeField] private Sprite bottomSingleColumnSprite;

	/*
	public override void RefreshTile (Vector3Int position, ITilemap tilemap)
	{
		base.RefreshTile (position, tilemap);
	}
	*/

	public override void GetTileData (Vector3Int position, ITilemap tilemap, ref TileData tileData)
	{
		//Debug.Log ("GetTileData: " + position);

		string left = HasGrass (tilemap, new Vector3Int (position.x - 1, position.y, position.z)) ? "L" : "-";
		string right = HasGrass (tilemap, new Vector3Int (position.x + 1, position.y, position.z)) ? "R" : "-";
		string top = HasGrass (tilemap, new Vector3Int (position.x, position.y + 1, position.z)) ? "T" : "-";
		string bottom = HasGrass (tilemap, new Vector3Int (position.x, position.y - 1, position.z)) ? "B" : "-";

		Sprite sprite = tileData.sprite;
		string neighbors = left + right + top + bottom;
		//Debug.Log (neighbors);

		switch (neighbors)
		{
		case "L---":
			tileData.sprite = rightSingleRowSprite;
			break;

		case "-R--":
			tileData.sprite = leftSingleRowSprite;
			break;

		case "LR--":
			tileData.sprite = middleSingleRowSprite;
			break;

		case "--T-":
			tileData.sprite = bottomSingleColumnSprite;
			break;

		case "---B":
			tileData.sprite = topSingleColumnSprite;
			break;

		case "--TB":
			tileData.sprite = middleSingleColumnSprite;
			break;

		case "LRT-":
			tileData.sprite = bottomSprite;
			break;

		case "LR-B":
			tileData.sprite = topSprite;
			break;

		case "LRTB":
			tileData.sprite = middleSprite;
			break;

		case "L-T-":
			tileData.sprite = bottomRightSprite;
			break;

		case "L--B":
			tileData.sprite = topRightSprite;
			break;

		case "L-TB":
			tileData.sprite = rightSprite;
			break;

		case "-RT-":
			tileData.sprite = bottomLeftSprite;
			break;

		case "-R-B":
			tileData.sprite = topLeftSprite;
			break;

		case "-RTB":
			tileData.sprite = leftSprite;
			break;

		default:
			tileData.sprite = singleSprite;
			break;
		}

		//base.GetTileData (position, tilemap, ref tileData);


	}

	private bool HasGrass (ITilemap tilemap, Vector3Int position)
	{
		return tilemap.GetTile (position) == this;
	}

	#if UNITY_EDITOR

	[MenuItem ("Assets/Create/Tiles/GrassTile")]
	public static void CreateGrassTile ()
	{
		string path = EditorUtility.SaveFilePanelInProject ("Save GrassTile", "New GrassTile", "asset", "Save GrassTile", "Assets");
		if (path == "")
		{
			return;
		}

		AssetDatabase.CreateAsset (ScriptableObject.CreateInstance<GrassTile> (), path);
	}

	#endif
}
