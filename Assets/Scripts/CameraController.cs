﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Transform heroSpawn;
	public HeroController hero;

	private Vector3 offset;

	public float minX = 0f;
	public float maxX = 80f;

	public float limitX = 6f;

	// Use this for initialization
	private void Start ()
	{
		offset = transform.position - heroSpawn.position;
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	private void OnEnable ()
	{
		Debug.Log ("CameraController:OnEnable");
		HeroSpawner.Instance.SelectHero += OnSelectHero;
	}

	private void OnDisable ()
	{
		if (HeroSpawner.Instance != null)
		{
			HeroSpawner.Instance.SelectHero -= OnSelectHero;
		}
	}

	private void OnSelectHero (HeroController hero)
	{
		this.hero = hero;
	}

	private void LateUpdate () 
	{
		float newPosX = Mathf.Clamp (hero.transform.position.x + offset.x, minX, maxX);

		float distanceX = transform.position.x - newPosX;
		//if (Mathf.Abs (distanceX) > limitX)
		{
			if (!hero.IsDead)
			{
				transform.position = new Vector3 (newPosX, transform.position.y, transform.position.z);
			}
		}
	}
}
