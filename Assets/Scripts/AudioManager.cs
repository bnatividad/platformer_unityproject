﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
	[SerializeField] private List<AudioSource> audioSources;
	[SerializeField] private int index = 0;

	[Space (10)]
	[SerializeField] private AudioClip button;
	[SerializeField] private AudioClip button2;
	[SerializeField] private AudioClip closeButton;
	[SerializeField] private AudioClip jump;
	[SerializeField] private AudioClip pause;
	[SerializeField] private AudioClip achievement;
	[SerializeField] private AudioClip coins;
	[SerializeField] private AudioClip die;
	[SerializeField] private AudioClip finish;

	[Space (10)]
	[SerializeField] private AudioSource jetpackAudioSource;
	[SerializeField] private AudioClip jetpack;

	// Use this for initialization
	private void Start ()
	{
		//AudioSource[] ass = GetComponents<AudioSource> ();
		//for (int i = 0; i < ass.Length; i++)
		//{
		//	audioSources.Add (ass [i]);
		//}
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	public void PlayButton ()
	{
		Play (button);
	}

	public void PlayButton2 ()
	{
		Play (button2);
	}

	public void PlayCloseButton ()
	{
		Play (closeButton);
	}

	public void PlayJump ()
	{
		Play (jump);
	}

	public void PlayPause ()
	{
		Play (pause);
	}

	public void PlayAchievement ()
	{
		Play (achievement);
	}

	public void PlayCoins ()
	{
		Play (coins);
	}

	public void PlayDie ()
	{
		Play (die);
	}

	public void PlayFinish ()
	{
		Play (finish);
	}

	public void PlayJetpack ()
	{
		if (!jetpackAudioSource.isPlaying)
		{
			jetpackAudioSource.Play ();
		}
	}

	public void StopJetpack ()
	{
		if (jetpackAudioSource.isPlaying)
		{
			jetpackAudioSource.Stop ();
		}
	}

	private void Play (AudioClip audioClip)
	{
		int startIndex = index;

		do
		{
			if (!audioSources [index].isPlaying)
			{
				audioSources[index].PlayOneShot (audioClip);
				index = (index + 1) % audioSources.Count;
				break;
			}
			index = (index + 1) % audioSources.Count;
		}
		while (true);
	}
}
