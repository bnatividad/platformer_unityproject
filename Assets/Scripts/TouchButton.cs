﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchButton : MonoBehaviour
, IPointerDownHandler
, IPointerUpHandler
{
	private CanvasGroup cg;

	public delegate void OnButtonPressed ();
	public event OnButtonPressed ButtonPressed;

	public delegate void OnButtonReleased ();
	public event OnButtonReleased ButtonReleased;

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	private void OnEnable ()
	{
		if (cg == null)
		{
			cg = GetComponent<CanvasGroup> ();
		}
		cg.alpha = 0.5f;
	}

	public void OnPointerDown (PointerEventData eventData)
	{
		if (ButtonPressed != null)
		{
			ButtonPressed ();
		}
		cg.alpha = 0.3f;
	}

	public void OnPointerUp (PointerEventData eventData)
	{
		if (ButtonReleased != null)
		{
			ButtonReleased ();
		}
		cg.alpha = 0.5f;
	}
}
