﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{
	protected const float minMoveDistance = 0.001f;
	protected const float shellRadius = 0.01f;

	public float minGroundNormalY = 0.65f;
	public float gravityModifier = 1f;

	protected bool isGrounded;
	protected Vector2 groundNormal;

	protected Vector2 targetVelocity;
	[SerializeField]
	protected Vector2 velocity;
	protected Rigidbody2D rb2d;

	protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
	protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D> ();
	protected ContactFilter2D contactFilter;

	private void OnEnable ()
	{
		rb2d = GetComponent<Rigidbody2D> ();
	}

	// Use this for initialization
	private void Start ()
	{
		contactFilter.useTriggers = false;
		contactFilter.SetLayerMask (Physics2D.GetLayerCollisionMask (gameObject.layer));
		contactFilter.useLayerMask = true;
	}
	
	// Update is called once per frame
	protected void Update ()
	{
		targetVelocity = Vector2.zero;
		ComputeVelocity ();
	}

	protected virtual void ComputeVelocity ()
	{
		// Override me
		Debug.Log ("PhysicsObject:ComputeVelocity");
	}

	private void FixedUpdate ()
	{
		Vector2 deltaPosition, moveAlongGround, move;

		if (gravityModifier > 0f)
		{
			velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
			velocity.x = targetVelocity.x;

			isGrounded = false;

			deltaPosition = velocity * Time.deltaTime;

			moveAlongGround = new Vector2 (groundNormal.y, -groundNormal.x);

			move = moveAlongGround * deltaPosition.x;
		}
		else
		{
			velocity.x = targetVelocity.x;

			isGrounded = false;

			deltaPosition = velocity * Time.deltaTime;

			move = deltaPosition;
		}

		Move (move, false);

		move = Vector2.up * deltaPosition.y;

		Move (move, true);
	}

	private void Move (Vector2 move, bool isYMovement)
	{
		float distance = move.magnitude;

		if (distance > minMoveDistance)
		{
			int hitCount = rb2d.Cast (move, contactFilter, hitBuffer, distance + shellRadius);

			hitBufferList.Clear ();
			for (int i = 0; i < hitCount; i++)
			{
				hitBufferList.Add (hitBuffer [i]);
			}

			for (int i = 0; i < hitBufferList.Count; i++)
			{
				Vector2 currentNormal = hitBufferList [i].normal;

				if (currentNormal.y > minGroundNormalY)
				{
					isGrounded = true;
					if (isYMovement)
					{
						groundNormal = currentNormal;
						currentNormal.x = 0;
					}
				}

				float projection = Vector2.Dot (velocity, currentNormal);
				if (projection < 0)
				{
					velocity = velocity - projection * currentNormal;
				}

				float modifiedDistance = hitBufferList [i].distance - shellRadius;
				distance = modifiedDistance < distance ? modifiedDistance : distance;
			}
		}

		if (rb2d == null)
		{
			rb2d = GetComponent<Rigidbody2D> ();
		}
		rb2d.position = rb2d.position + move.normalized * distance;
	}
}
