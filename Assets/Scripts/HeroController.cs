﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : PhysicsObject
{
	private SpriteRenderer spriteRenderer;
	private Animator animator;

	public float maxSpeed = 5;
	public float jumpSpeed = 10;

	[Space (10)]
	[SerializeField] private bool hasJumped = false;
	[SerializeField] private bool canDoubleJump = false;
	[SerializeField] private bool hasDoubleJumped = false;
	[SerializeField] private bool canJetpack = false;
	[SerializeField] private float jetpackFuel = 0f;
	[SerializeField] private float jetpackFuelMax = 10f;

	[Space(10)]
	[SerializeField] private bool isDead = false;

	[Space(10)]
	public Vector3 move;

	[SerializeField] private bool isBtnLeftPressed = false;
	[SerializeField] private bool isBtnRightPressed = false;
	[SerializeField] private bool isBtnJumpPressed = false;
	[SerializeField] private bool isBtnJumpJustPressed = false;

	private void Awake ()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
		animator = GetComponent<Animator> ();
	}

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		base.Update ();	
	}

	private void OnEnable ()
	{
		HUDCanvas.Instance.btnLeft.ButtonPressed += OnBtnLeftPressed;
		HUDCanvas.Instance.btnLeft.ButtonReleased += OnBtnLeftReleased;

		HUDCanvas.Instance.btnRight.ButtonPressed += OnBtnRightPressed;
		HUDCanvas.Instance.btnRight.ButtonReleased += OnBtnRightReleased;

		HUDCanvas.Instance.btnJump.ButtonPressed += OnBtnJumpPressed;
		HUDCanvas.Instance.btnJump.ButtonReleased += OnBtnJumpReleased;
	}

	private void OnDisable ()
	{
		if (HUDCanvas.Instance != null)
		{
			HUDCanvas.Instance.btnLeft.ButtonPressed -= OnBtnLeftPressed;
			HUDCanvas.Instance.btnLeft.ButtonReleased -= OnBtnLeftReleased;

			HUDCanvas.Instance.btnRight.ButtonPressed -= OnBtnRightPressed;
			HUDCanvas.Instance.btnRight.ButtonReleased -= OnBtnRightReleased;

			HUDCanvas.Instance.btnJump.ButtonPressed -= OnBtnJumpPressed;
			HUDCanvas.Instance.btnJump.ButtonReleased -= OnBtnJumpReleased;
		}
	}

	protected override void ComputeVelocity ()
	{
		if (HUDCanvas.Instance.isActiveAndEnabled && !isDead)
		{
			#if UNITY_EDITOR
			if (Input.GetKeyDown (KeyCode.LeftArrow))
			{
				isBtnLeftPressed = true;
				AchievementManager.Instance.SendAction ("input");
			}
			if (Input.GetKeyUp (KeyCode.LeftArrow))
			{
				isBtnLeftPressed = false;
			}
			if (Input.GetKeyDown (KeyCode.RightArrow))
			{
				isBtnRightPressed = true;
				AchievementManager.Instance.SendAction ("input");
			}
			if (Input.GetKeyUp (KeyCode.RightArrow))
			{
				isBtnRightPressed = false;
			}
			if (Input.GetKeyDown (KeyCode.Space))
			{
				isBtnJumpPressed = true;
				isBtnJumpJustPressed = true;
				AchievementManager.Instance.SendAction ("input");
			}
			if (Input.GetKeyUp (KeyCode.Space))
			{
				isBtnJumpPressed = false;
			}
			#endif

			if (isBtnLeftPressed)
			{
				move.x = Mathf.Clamp (move.x - 0.1f, -1f, 1f);
			}
			else if (isBtnRightPressed)
			{
				move.x = Mathf.Clamp (move.x + 0.1f, -1f, 1f);
			}
			else
			{
				if (Mathf.Approximately (move.x, 0f))
				{
					move.x = 0f;
				}
				else if (move.x > 0f)
				{
					move.x = Mathf.Clamp (move.x - 0.1f, 0f, 1f);
				}
				else
				{
					move.x = Mathf.Clamp (move.x + 0.1f, -1f, 0f);
				}
			}

			//	move.x = actualMoveX;

			if (isBtnJumpPressed)
			{
				if (isBtnJumpJustPressed)
				{
					if (isGrounded && !hasJumped)
					{
						velocity.y = jumpSpeed;
						hasJumped = true;

						AudioManager.Instance.PlayJump ();
					}
					else if (!isGrounded && canDoubleJump && !hasDoubleJumped)
					{
						velocity.y = jumpSpeed;
						hasDoubleJumped = true;

						AudioManager.Instance.PlayJump ();
					}
				}
				else
				{
					if (canJetpack)
					{
						if (!isGrounded && jetpackFuel > 0)
						{
							velocity.y = jumpSpeed;

							AudioManager.Instance.PlayJetpack ();
						}
						else
						{
							AudioManager.Instance.StopJetpack ();
						}
					}
				}
			}
			else
			{
				if (velocity.y > 0f)
				{
					velocity.y = velocity.y * 0.5f;
				}

				if (canJetpack)
				{
					AudioManager.Instance.StopJetpack ();
				}
			}

			if (canJetpack)
			{
				HUDCanvas.Instance.SetFuel (jetpackFuel / jetpackFuelMax);
			}

			//if (Input.GetButton ("Jump"))
			//{
			//	Debug.Log ("IsGrounded: " + isGrounded.ToString () + " " + Time.time);
			//}

			if (isGrounded)
			{
				if (move.x != 0f)
				{
					animator.Play ("Run");	
				}
				else
				{
					animator.Play ("Idle");
				}
			}
			else
			{
				if (velocity.y >= 0f)
				{
					animator.Play ("Jump");
				}
				else
				{
					animator.Play ("Fall");
				}
			}

			transform.localScale = new Vector3 ((move.x < 0f ? -1f : 1f), 1f, 1f);
		}
		else
		{
			isBtnLeftPressed = false;
			isBtnRightPressed = false;
			isBtnJumpPressed = false;
			isBtnJumpJustPressed = false;
		}

		targetVelocity = move * maxSpeed;
	}

	private void LateUpdate ()
	{
		isBtnJumpJustPressed = false;

		if (canJetpack)
		{
			if (isBtnJumpPressed)
			{
				jetpackFuel = Mathf.Clamp (jetpackFuel - (5f * Time.deltaTime), 0f, jetpackFuelMax);
			}
			else
			{
				if (isGrounded)
				{
					jetpackFuel = Mathf.Clamp (jetpackFuel + (3f * Time.deltaTime), 0f, jetpackFuelMax);
				}
				else
				{
					jetpackFuel = Mathf.Clamp (jetpackFuel + (1.5f * Time.deltaTime), 0f, jetpackFuelMax);
				}
			}
		}

		if (isGrounded)
		{
			hasJumped = false;
			hasDoubleJumped = false;
		}
	}

	private void OnBtnLeftPressed()
	{
		isBtnLeftPressed = true;
		isBtnRightPressed = false;
	}

	private void OnBtnLeftReleased()
	{
		isBtnLeftPressed = false;
	}

	private void OnBtnRightPressed ()
	{
		isBtnRightPressed = true;
		isBtnLeftPressed = false;
	}

	private void OnBtnRightReleased ()
	{
		isBtnRightPressed = false;
	}

	private void OnBtnJumpPressed ()
	{
		isBtnJumpPressed = true;
		isBtnJumpJustPressed = true;
	}

	private void OnBtnJumpReleased ()
	{
		isBtnJumpPressed = false;
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		//Debug.Log ("Hero::OnTriggerEnter2D: " + other.name);

		switch (other.tag)
		{
		case "Coin":
			other.gameObject.SetActive (false);
			HUDCanvas.Instance.CollectCoin ();

			AchievementManager.Instance.SendAction ("coin");
			AudioManager.Instance.PlayCoins ();
			break;

		case "DeadZone":
			HUDCanvas.Instance.Hide ();
			Invoke ("GameOver", 1f);

			AudioManager.Instance.PlayDie ();
			//GameOverCanvas.Instance.Show (true, HUDCanvas.Instance.Coins, HUDCanvas.Instance.FormattedTime);
			break;

		case "Flag":
			Invoke ("YouWin", 1f);

			AudioManager.Instance.PlayFinish ();
			//GameOverCanvas.Instance.Show (false, HUDCanvas.Instance.Coins, HUDCanvas.Instance.FormattedTime);
			break;
		}
	}

	private void OnCollisionEnter2D (Collision2D other)
	{
		//Debug.Log ("OnCollisionEnter2D: " + other.gameObject.name);

		switch (other.gameObject.tag)
		{
		case "Enemy":
			Die ();
			break;
		}
	}

	public void Reset ()
	{
		isDead = false;

		move = Vector3.zero;
		jetpackFuel = jetpackFuelMax;
		AudioManager.Instance.StopJetpack ();

		BoxCollider2D collider = GetComponent<BoxCollider2D> ();
		collider.enabled = true;
	}

	public void Die ()
	{
		Debug.Log ("HeroController:Die");

		isDead = true;

		BoxCollider2D collider = GetComponent<BoxCollider2D> ();
		collider.enabled = false;

		velocity = new Vector3 (0f, 8f, 0f);

		animator.Play ("Fall");

		if (canJetpack)
		{
			AudioManager.Instance.StopJetpack ();
		}

		Invoke ("GameOver", 1f);

		AudioManager.Instance.PlayDie ();
	}

	public void GameOver ()
	{
		HUDCanvas.Instance.Hide ();

		string coins = HUDCanvas.Instance.Coins;
		string time = HUDCanvas.Instance.FormattedTime;
		GameOverCanvas.Instance.Show (true, coins, time);
	}

	public void YouWin ()
	{
		HUDCanvas.Instance.Hide ();

		string coins = HUDCanvas.Instance.Coins;
		string time = HUDCanvas.Instance.FormattedTime;
		GameOverCanvas.Instance.Show (false, coins, time);
	}

	public bool IsDead
	{
		get { return isDead; }
	}

	public bool CanJetpack
	{
		get { return canJetpack; }
	}
}
