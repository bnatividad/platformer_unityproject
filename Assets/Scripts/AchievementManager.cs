﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementManager : Singleton<AchievementManager>
{
	[SerializeField] private List<Action> actions;
	[SerializeField] private List<Achievement> achievements;

	// Use this for initialization
	private void Start ()
	{
			
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	public void SendAction (string actionName)
	{
		int actionIndex = -1;
		for (int i = 0; i < actions.Count; i++)
		{
			Action action = actions [i];
			if (action.name == actionName)
			{
				action.count++;
				if (action.count >= action.required)
				{
					Achievement achievement = FindAchievement (action.achievementName);
					AchievementCanvas.Instance.Show (achievement.sprite, achievement.title, achievement.description);
					actionIndex = i;
					break;
				}
			}
		}

		if (actionIndex >= 0)
		{
			actions.RemoveAt (actionIndex);
		}
	}

	private Achievement FindAchievement (string achievementName)
	{
		for (int i = 0; i < achievements.Count; i++)
		{
			if (achievements [i].name == achievementName)
			{
				return achievements[i];
			}
		}

		return null;
	}
}

[System.Serializable]
public class Action
{
	public string name;
	public int count;
	public int required;
	public string achievementName;
}

[System.Serializable]
public class Achievement
{
	public string name;
	public Sprite sprite;
	public string title;
	public string description;
}
