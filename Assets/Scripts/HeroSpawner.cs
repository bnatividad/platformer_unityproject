﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroSpawner : Singleton<HeroSpawner>
{
	[SerializeField] private List<HeroController> heroes;
	[SerializeField] private HeroController selectedHero;

	public delegate void OnSelectHero (HeroController hero);
	public event OnSelectHero SelectHero;

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	private void OnEnable ()
	{
		Debug.Log ("HeroSpawner:OnEnable");
		int selectedHeroIndex = PlayerPrefs.GetInt ("SelectedHero", 0);
		selectedHero = heroes [selectedHeroIndex];

		SpawnHero (selectedHero);
	}

	public void SpawnHero (HeroController selectedHero)
	{
		for (int i = 0; i < heroes.Count; i++)
		{
			HeroController hero = heroes [i];

			if (hero == selectedHero)
			{
				hero.gameObject.SetActive (true);
				hero.transform.position = transform.position;
				hero.Reset ();

				this.selectedHero = hero;

				if (SelectHero != null)
				{
					SelectHero (hero);
				}
			}
			else
			{
				hero.gameObject.SetActive (false);
			}
		}
	}

	public void Reset ()
	{
		SpawnHero (selectedHero);
	}

	public HeroController SelectedHero
	{
		get { return selectedHero; }
	}
}
