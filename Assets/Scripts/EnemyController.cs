﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : PhysicsObject
{
	public float speed = 2f;
	public float directionX = -1f;

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		base.Update ();	
	}

	protected override void ComputeVelocity ()
	{
		if (HUDCanvas.Instance.isActiveAndEnabled || GameOverCanvas.Instance.isActiveAndEnabled)
		{
			Vector3 move = new Vector3 (directionX, 0f, 0f);

			transform.localScale = new Vector3 ((move.x < 0f ? -2f : 2f), 2f, 1f);

			targetVelocity = move * speed;
		}
	}


	private void OnTriggerEnter2D (Collider2D other)
	{
		//Debug.Log ("Enemy::OnTriggerEnter2D: " + other.name);

		switch (other.tag)
		{
		case "EnemyFlipper":
			directionX = directionX * -1f;
			break;
		}
	}

	public void Reset ()
	{
		transform.localPosition = Vector3.zero;
		directionX = -1f;	
	}
}
