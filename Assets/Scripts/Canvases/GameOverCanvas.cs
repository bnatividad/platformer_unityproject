﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverCanvas : BaseCanvas<GameOverCanvas>
{
	[SerializeField] private Text txtTitle;

	[Space (10)]
	[SerializeField] private Text txtCoins;
	[SerializeField] private Text txtTime;

	[SerializeField] private Button btnReplay;
	[SerializeField] private Button btnHome;

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	private void OnEnable ()
	{
		
	}

	public void OnBtnReplay ()
	{
		Hide ();

		HUDCanvas.Instance.Reset ();
		HeroSpawner.Instance.Reset ();

		HUDCanvas.Instance.Show ();

		AchievementManager.Instance.SendAction ("start_game");

		AudioManager.Instance.PlayButton ();
	}

	public void OnBtnHome ()
	{
		Hide ();

		MainCanvas.Instance.Show ();

		AudioManager.Instance.PlayButton ();
	}

	public void Show (bool isGameOver, string coins, string time)
	{
		if (isGameOver)
		{
			txtTitle.text = "GAME OVER";
			txtCoins.text = coins;
			txtTime.text = time;
		}
		else
		{
			txtTitle.text = "FINISH!";
			txtCoins.text = coins;
			txtTime.text = time;
		}

		base.Show ();
	}
}
