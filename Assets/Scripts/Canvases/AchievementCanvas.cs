﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementCanvas : BaseCanvas<AchievementCanvas>
{
	[SerializeField] private RectTransform panel;
	[SerializeField] private Image icon;
	[SerializeField] private Text txtTitle;
	[SerializeField] private Text txtDescription;

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	private void OnEnable ()
	{
		//Test
		//Show ("Title", "Description");
	}

	public void Show (Sprite sprite, string title, string description)
	{
		icon.sprite = sprite;
		txtTitle.text = title;
		txtDescription.text = description;

		base.Show ();

		StartCoroutine ("IShow2");
	}

	private IEnumerator IShow2 ()
	{
		panel.anchoredPosition = new Vector2 (0f, panel.anchoredPosition.y + panel.sizeDelta.y);

		do
		{
			yield return new WaitForEndOfFrame();
			panel.anchoredPosition = new Vector2 (0f, panel.anchoredPosition.y - 10f);
		}
		while (panel.anchoredPosition.y > -10f);

		yield return new WaitForEndOfFrame();

		panel.anchoredPosition = new Vector2 (0f, -10f);

		AudioManager.Instance.PlayAchievement ();

		Invoke ("Hide", 4f);
	}

	public override void Hide ()
	{
		base.Hide ();
		StartCoroutine ("IHide2");
	}

	private IEnumerator IHide2 ()
	{
		panel.anchoredPosition = new Vector2 (0f, -10f);

		do
		{
			yield return new WaitForEndOfFrame ();
			panel.anchoredPosition = new Vector2 (0f, panel.anchoredPosition.y + 10f);
		}
		while (panel.anchoredPosition.y < panel.sizeDelta.y + 10);

		yield return new WaitForEndOfFrame();

		panel.anchoredPosition = new Vector2 (0f, panel.sizeDelta.y + 10);
	}
}
