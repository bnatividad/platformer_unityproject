﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectHeroCanvas : BaseCanvas<SelectHeroCanvas> 
{
	[SerializeField] private List<HeroController> heroes;
	[SerializeField] private List<Button> btnHeroes;

	private HeroController selectedHero;
	private int selectedHeroIndex = 0;

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	private void OnEnable ()
	{
		selectedHeroIndex = PlayerPrefs.GetInt ("SelectedHero", 0);

		OnBtnHero (selectedHeroIndex);
	}

	public void OnBtnClose ()
	{
		Hide ();

		AudioManager.Instance.PlayCloseButton ();
	}

	public void OnBtnHero (int index)
	{
		for (int i = 0; i < btnHeroes.Count; i++)
		{
			Button btnHero = btnHeroes [i];
			HeroController hero = heroes [i];

			if (btnHero.interactable)
			{
				Image image = btnHero.GetComponent<Image> ();
				if (i == index)
				{
					image.color = new Color (30f / 255f, 244f / 255f, 60f / 255f);

					selectedHeroIndex = index;

					AudioManager.Instance.PlayButton2 ();
				}
				else
				{
					image.color = Color.white;
				}
			}
		}
	}

	public void OnBtnSelect ()
	{
		Hide ();

		PlayerPrefs.SetInt ("SelectedHero", selectedHeroIndex);
		HeroController hero = heroes [selectedHeroIndex];

		HeroSpawner.Instance.SpawnHero (hero);

		AudioManager.Instance.PlayButton ();
	}
}
