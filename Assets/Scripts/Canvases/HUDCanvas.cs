﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDCanvas : BaseCanvas<HUDCanvas>
{
	private const float GAME_TIME = 120f;

	[SerializeField] private Text txtCoins;
	[SerializeField] private int coins;

	[SerializeField] private Text txtTime;
	[SerializeField] private float time = GAME_TIME;

	public TouchButton btnLeft;
	public TouchButton btnRight;
	public TouchButton btnJump;

	[SerializeField] private GameObject jetpackPanel;
	[SerializeField] private RectTransform jetpackFuel;

	[Header ("Coins")] // WIP
	[SerializeField] private List<GameObject> coinObjects;

	[Header ("Enemies")] // WIP
	[SerializeField] private List<EnemyController> enemies;

	// Use this for initialization
	private void Start ()
	{
		
	}

	private void OnApplicationPause (bool isPause)
	{
		if (!PauseCanvas.Instance.isActiveAndEnabled)
		{
			Time.timeScale = 0f;
			PauseCanvas.Instance.Show ();
		}
	}

	// Update is called once per frame
	private void Update ()
	{
		#if UNITY_EDITOR
		if (Input.GetKeyDown (KeyCode.P))
		{
			OnBtnPause ();
		}
		#endif

		time = Mathf.Clamp (time - Time.deltaTime, 0f, GAME_TIME);
		txtTime.text = GetFormattedTime (time);

		if (time <= 0f)
		{
			if (!HeroSpawner.Instance.SelectedHero.IsDead)
			{
				HeroSpawner.Instance.SelectedHero.Die ();
			}
		}
	}

	public override void Show ()
	{
		base.Show ();

		if (HeroSpawner.Instance.SelectedHero.CanJetpack)
		{
			jetpackPanel.gameObject.SetActive (true);
		}
		else
		{
			jetpackPanel.gameObject.SetActive (false);
		}
	}

	public void Reset ()
	{
		coins = 0;
		txtCoins.text = "x " + coins.ToString ();

		time = 120f;
		txtTime.text = GetFormattedTime (time);

		for (int i = 0; i < coinObjects.Count; i++)
		{
			GameObject coin = coinObjects [i];
			coin.SetActive (true);
		}

		for (int i = 0; i < enemies.Count; i++)
		{
			EnemyController enemy = enemies [i];
			enemy.Reset ();
		}
	}

	private string GetFormattedTime (float time)
	{
		int t = Mathf.CeilToInt (time);
		string formattedTime = (t / 60).ToString ("D2") + ":" + (t % 60).ToString ("D2");

		return formattedTime;
	}

	public void CollectCoin ()
	{
		coins++;
		txtCoins.text = "x " + coins.ToString ();
	}

	public string Coins
	{
		get { return coins.ToString (); }
	}

	public string FormattedTime
	{
		get { return GetFormattedTime (time); }
	}

	public void OnBtnPause ()
	{
		PauseCanvas.Instance.Show ();

		AudioManager.Instance.PlayButton ();
	}

	public void SetFuel (float percent)
	{
		jetpackPanel.gameObject.SetActive (true);
		jetpackFuel.sizeDelta = new Vector2 (160f * percent, 10f);
	}
}
