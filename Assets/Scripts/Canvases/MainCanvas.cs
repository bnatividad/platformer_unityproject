﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCanvas : BaseCanvas<MainCanvas>
{
	
	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	public void OnBtnSelectHero ()
	{
		SelectHeroCanvas.Instance.Show ();

		AudioManager.Instance.PlayButton ();
	}

	public void OnBtnPlay ()
	{
		Hide ();

		HUDCanvas.Instance.Reset ();
		HeroSpawner.Instance.Reset ();

		HUDCanvas.Instance.Show ();

		AchievementManager.Instance.SendAction ("start_game");

		AudioManager.Instance.PlayButton ();
	}

	public void OnBtnSoundOn ()
	{
	}

	public void OnBtnSoundOff ()
	{
	}

	public void OnBtnTemp ()
	{
		AudioManager.Instance.PlayButton ();
	}
}
