﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseCanvas : BaseCanvas<PauseCanvas>
{

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	public override void Show ()
	{
		base.Show ();

		AudioManager.Instance.PlayPause ();
	}

	public void OnBtnResume ()
	{
		Hide ();

		Time.timeScale = 1f;

		AudioManager.Instance.PlayButton ();
	}

	public void OnBtnHome ()
	{
		Hide ();

		HUDCanvas.Instance.Hide ();

		Time.timeScale = 1f;
		MainCanvas.Instance.Show ();

		AudioManager.Instance.PlayButton ();
	}
}
