﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseCanvas<T> : MonoBehaviour where T : MonoBehaviour
{
	private CanvasGroup cg;
	private GraphicRaycaster gr;

	private static T instance;

	public static T Instance
	{
		get
		{
			if (instance == null)
			{
				Object[] objects = Resources.FindObjectsOfTypeAll (typeof(T));
				if (objects.Length > 0)
				{
					instance = (T) objects [0];
				}
			}

			return instance;
		}
	}

	// Use this for initialization
	private void Start ()
	{
		
	}
	
	// Update is called once per frame
	private void Update ()
	{
		
	}

	private void OnEnable ()
	{
		if (cg == null)
		{
			cg = GetComponent<CanvasGroup> ();
		}
		if (gr == null)
		{
			gr = GetComponent<GraphicRaycaster> ();
		}
	}

	public virtual void Show ()
	{
		gameObject.SetActive (true);

		StopCoroutine ("IShow");
		StartCoroutine ("IShow");
	}

	private IEnumerator IShow ()
	{
		if (cg == null)
			cg = GetComponent<CanvasGroup> ();
		if (gr == null)
			gr = GetComponent<GraphicRaycaster> ();
		
		cg.alpha = 0f;
		gr.enabled = false;

		do
		{
			//yield return new WaitForSecondsRealtime (0.1f);
			yield return new WaitForEndOfFrame ();
			cg.alpha = Mathf.Clamp (cg.alpha + 0.1f, 0f, 1f);
		}
		while (cg.alpha < 1f);

		yield return new WaitForEndOfFrame ();

		cg.alpha = 1f;
		gr.enabled = true;
	}

	public virtual void Hide ()
	{
		if (gameObject.activeInHierarchy)
		{
			StopCoroutine ("IHide");
			StartCoroutine ("IHide");
		}
	}

	private IEnumerator IHide ()
	{
		if (cg == null)
			cg = GetComponent<CanvasGroup> ();
		if (gr == null)
			gr = GetComponent<GraphicRaycaster> ();
		
		cg.alpha = 1f;
		gr.enabled = false;

		do
		{
			//yield return new WaitForSecondsRealtime (0.1f);
			yield return new WaitForEndOfFrame ();
			cg.alpha = Mathf.Clamp (cg.alpha - 0.1f, 0f, 1f);
		}
		while (cg.alpha > 0f);

		yield return new WaitForSeconds (0.1f);

		cg.alpha = 0f;
		gr.enabled = false;

		gameObject.SetActive (false);
	}
}
